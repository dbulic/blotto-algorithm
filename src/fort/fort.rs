

pub struct Fort {
    points: u8,
    soldiers: u8
}

impl Copy for Fort { }

impl Clone for Fort {
    fn clone(&self) -> Fort {
        *self
    }
}

impl Fort {
    pub fn new(_points: u8) -> Fort{
        Fort{
            points: _points,
            soldiers: 0
        }
    }

    pub fn get_points(&self) -> u8 {
        self.points
    }

    pub fn get_soldiers(&self) -> u8 {
        self.soldiers
    }

    pub fn set_soldiers(&mut self, _soldiers: u8) {
        self.soldiers = _soldiers;
    }
}