#![allow(warnings)]

mod player;
mod fort;
use player::Player;
use std::io::{Write, stdout, stdin};
use crossterm::{QueueableCommand, cursor};
use colored::*;

fn main() {
    let mut stdout = stdout();
    let mut buffer: String = String::new();
    let mut buffer2: String = String::new();
    let stdin = stdin();
    println!("How many players?");
    stdin.read_line(&mut buffer);
    let num_players = buffer.trim().parse::<usize>().unwrap();
    
    println!("How many generations?");
    stdin.read_line(&mut buffer2);
    let num_gen = buffer2.trim().parse::<usize>().unwrap();
    let mut generation: Vec<Player> = vec![];

    for _i in 0..num_players {
        let mut player1 = Player::new();
        player1.calc_random_strat();
        generation.push(player1);
    }
    println!();

    for gen in 0..=num_gen {
        for i in 0..num_players {
            for j in i..num_players {
                if i != j{
                    let mut win1: u8 = 0;
                    let mut win2: u8 = 0;
                    for k in 0..10 {
                        // println!("{}", generation[i].print_strat());
                        if generation[i].get_soldiers_from_fort(k) > generation[j].get_soldiers_from_fort(k) {
                            win1 += generation[i].get_points_from_fort(k);
                        }
                        else if generation[i].get_soldiers_from_fort(k) < generation[j].get_soldiers_from_fort(k) {
                            win2 += generation[j].get_points_from_fort(k);
                        }
                        else {
                            win1 += generation[i].get_points_from_fort(k)/2;
                            win2 += generation[j].get_points_from_fort(k)/2;
                        }
                    }
                    generation[i].add_score(win1 as u32);
                    generation[j].add_score(win2 as u32);                    
                }
            }
        }
        let mut best: Player = Player::new();
        for i_pl in 0..num_players {
            if generation[i_pl].get_score() > best.get_score() {
                best.set_forts(generation[i_pl].get_forts());
                best.set_score(generation[i_pl].get_score());
            }
        }
        let out = String::from(format!("  >"));
        let out1 = String::from(format!("Best strategy:"));
        let out2 = String::from(format!("{}", best.print_strat()));
        let out3 = String::from(format!(" completed = {:3.02}%", gen as f32/num_gen as f32*100 as f32));
        
        stdout.queue(cursor::SavePosition);
        stdout.write(format!("{} {} {} {}", out, out1.green(), out2.yellow().bold(), out3.blue()).as_bytes());
        stdout.queue(cursor::RestorePosition);
        stdout.flush();
        // println!("{} {} {} {}", out, out1, out2.bold(), out3.blue());

        generation.clear();

        for _i in 0..num_players {
            let player = Player::new_mutant_from_player(best.get_forts());
            generation.push(player);
        }
    }
    println!();

    

}
