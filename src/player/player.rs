use crate::fort::Fort;
use rand::Rng;
use rand::thread_rng;
use std::fmt::Write;


pub struct Player {
    forts: Vec<Fort>,
    score: u32
}

impl Player {
    pub fn new() -> Player{
        Player {
            forts: vec![
                Fort::new(25),
                Fort::new(14),
                Fort::new(8),
                Fort::new(3),
                Fort::new(1),
                Fort::new(1),
                Fort::new(3),
                Fort::new(8),
                Fort::new(14),
                Fort::new(25)
            ],
            // forts: vec![
            //     Fort::new(10),
            //     Fort::new(10),
            //     Fort::new(10),
            //     Fort::new(10),
            //     Fort::new(10),
            //     Fort::new(10),
            //     Fort::new(10),
            //     Fort::new(10),
            //     Fort::new(10),
            //     Fort::new(10)
            // ],
            score: 0
        }
    }

    pub fn get_forts(&self) ->  Vec<Fort> {
        self.forts.clone()
    }

    pub fn set_forts(&mut self, _forts:Vec<Fort>) {
        self.forts = _forts;
    }

    pub fn calc_random_strat(&mut self) {
        let mut total_soldiers: u8 = 100;
        let mut rng = thread_rng();
        for i in 0..9 {
            let v = rng.gen_range(0..=total_soldiers);
            self.forts[i].set_soldiers(v);
            total_soldiers-=v;
        }
        self.forts[9].set_soldiers(total_soldiers);
    }

    pub fn new_mutant_from_player( _best: Vec<Fort>) -> Player{
        let mut player = Player::new();
        let mut total_soldiers: u8 = 100;
        let mut rng = thread_rng();
        for i in 0..9 {
            let v = rng.gen_range(0.9..=1.1);
            let cur = (_best[i].get_points() as f32 * v).round();
            let sol: u8 = if cur <= 0.0 {0} else {cur as u8};
            player.forts[i as usize].set_soldiers(sol);
            total_soldiers-= sol;
        }
        player.forts[9].set_soldiers(total_soldiers);
        player
    }

    pub fn get_points_from_fort(&self, i: usize) -> u8 {
        self.forts[i].get_points()
    }

    pub fn get_score(&self) -> u32 {
        self.score
    }

    pub fn get_soldiers_from_fort(&self, i: usize) -> u8 {
        self.forts[i].get_soldiers()
    }

    pub fn add_score(&mut self, points: u32) {
        self.score += points;
    }

    pub fn set_score(&mut self, points: u32) {
        self.score = points;
    }

    pub fn print_strat(&self) -> String {
        let mut out: String = String::from("");
        for fort in &self.forts {
            write!(out, "{:3},", fort.get_soldiers());
        }
        out
    }
}